package vpcisactoritemspanels;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import gui.Launcher;
import vpcispanels.AllAbstractionLevelsPanel;
import vpcispanels.TemplatePanel;

public class AllActivityActorsPanel extends TemplatePanel {

	public AllActivityActorsPanel() {
		super("List of All Actor Activities ");
		// TODO Auto-generated constructor stub

		// hide the useless buttons
		updateButton.hide();
		deleteButton.hide();
		cancelButton.hide();
		okButton.hide();

		// configure the buttons
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// do something
			}
		});

		editButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// do something
			}
		});

		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// do something
			}
		});

		// centerpanel
		centerPanel.setLayout(new FlowLayout());
		
		try {

			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			String request = String.format("SELECT * FROM ActivityActor " + "WHERE idAbstractionLevel="
					+ AllAbstractionLevelsPanel.idAbstractionLevelEntred + "");
			// System.out.println(request);
			Launcher.adbc.rs = Launcher.adbc.st.executeQuery(request);

			ImageIcon activityActorIcon = new ImageIcon("icons//ActivityActor.png");

			if (!Launcher.adbc.rs.next()) {
				centerPanel.add(new JLabel("No Actor Activities found"));
			} else {

				// becuase we moved forward in the if statement above
				Launcher.adbc.rs.previous();

				while (Launcher.adbc.rs.next()) {
					// show all Teams
					// add mouseListener on each Label

					int idActivityActor = Launcher.adbc.rs.getInt("idActivityActor");
					// System.out.println(AbstractionLevelId);
					String nameActivityActor = Launcher.adbc.rs.getString("nameActivityActor");
					// System.out.println(AbstractionLevelName);
					String quality = Launcher.adbc.rs.getString("quality");
					final JLabel activityActorLabel = new JLabel(idActivityActor + " : " + nameActivityActor + " (" + quality + ")", activityActorIcon, JLabel.LEFT);
					
					centerPanel.add(activityActorLabel);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
