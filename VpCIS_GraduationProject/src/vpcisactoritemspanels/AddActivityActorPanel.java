package vpcisactoritemspanels;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import vpcispanels.AllAbstractionLevelsPanel;
import vpcispanels.TemplatePanel;

public class AddActivityActorPanel extends TemplatePanel {
	JLabel nameActivityActorLabel;
	JTextField nameActivityActorTextField;

	JLabel idActivityTeamLabel;
	JList idActivityTeamList;

	JLabel idAbstractionLevelLabel;
	JTextField idAbstractionLevelTextField;

	public AddActivityActorPanel() {
		super("Add an Actor Activity ");
		// TODO Auto-generated constructor stub

		// hide the useless buttons
		addButton.hide();
		updateButton.hide();
		deleteButton.hide();
		backButton.hide();
		editButton.hide();

		// configure the centerPanel
		centerPanel.setLayout(new FlowLayout());

		nameActivityActorLabel = new JLabel("Name :");
		nameActivityActorTextField = new JTextField();
		nameActivityActorTextField.setColumns(20);

		idActivityTeamLabel = new JLabel("From Activity Team :");
		idActivityTeamList = new JList();

		idAbstractionLevelLabel = new JLabel("Id Abstraction Level Related :");
		idAbstractionLevelTextField = new JTextField();
		idAbstractionLevelTextField.setColumns(20);
		idAbstractionLevelTextField.setEditable(false);
		idAbstractionLevelTextField.setText(String.valueOf(AllAbstractionLevelsPanel.idAbstractionLevelEntred));

		// show system message
		showSystemMessage("Name field is mendatory");
		// add to the centerPanel
		centerPanel.add(nameActivityActorLabel);
		centerPanel.add(nameActivityActorTextField);

		centerPanel.add(idActivityTeamLabel);
		centerPanel.add(new JScrollPane(idActivityTeamList));
		

		centerPanel.add(idAbstractionLevelLabel);
		centerPanel.add(idAbstractionLevelTextField);
		
		// configure the buttons
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						// do something
					}
				});
				
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						// do something
					}
				});

	}

}
