package vpcisactoritemspanels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import vpcispanels.TemplatePanel;

public class EditActivityActorPanel extends TemplatePanel {

	protected JLabel idActivityActorLabel;
	protected JList allActivityActorsList;
	protected JLabel nameActivityActorLabel;
	protected JTextField nameActivityActorTextField;
	// protected JLabel idActivityActorLabel;
	// protected JList allActivityActorsList;

	protected JLabel idAbstractionLevelLabel;
	protected JTextField idAbstractionLevelTextField;
	// protected ArrayList<ActivityTeamRecord> activityTeams = new ArrayList<();

	public EditActivityActorPanel() {
		super("Edit an existing Actor Activity");
		// TODO Auto-generated constructor stub

		// hide useless buttons
		okButton.hide();
		addButton.hide();
		backButton.hide();
		editButton.hide();

		// configure the centerPanel
		idActivityActorLabel = new JLabel("Id Activity Actor :");
		allActivityActorsList = new JList();
		nameActivityActorLabel = new JLabel("Name Activity Actor :");
		nameActivityActorTextField = new JTextField();
		nameActivityActorTextField.setColumns(20);
		// idActivityActorLabel = new JLabel("Id Activity Actor :");
		// allActivityActorsList = new JList();
		idAbstractionLevelLabel = new JLabel("Id Abstarction Level related :");
		idAbstractionLevelTextField = new JTextField();
		idAbstractionLevelTextField.setColumns(20);
		idAbstractionLevelTextField.setEditable(false);

		// showSystemMessage
		showSystemMessage(
				"Select an Id of an Activity Actor. To update,choose an Id Activity Actor, change the name then click the 'Update' Button,"
						+ " to delete, simply choose the Id Activity actor then click 'Delete' Button");
		// fill the Jlist

		// add components to the centerpPanel

				centerPanel.add(idActivityActorLabel);
				centerPanel.add(new JScrollPane(allActivityActorsList));
				centerPanel.add(nameActivityActorLabel);
				centerPanel.add(nameActivityActorTextField);
				centerPanel.add(idAbstractionLevelLabel);
				centerPanel.add(idAbstractionLevelTextField);
				
				
				//configuration of the button
				updateButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						// do something
					}
				});

				deleteButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						// do something
					}
				});
				
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						// do something
					}
				});
				
				class ActivityActorRecord {

					int idActivityActor;
					String nameActivityActor;
					int idAbstractionLevel;

					ActivityActorRecord(int idActivityActor, String nameActivityActor, int idAbstractionLevel) {
						this.idActivityActor = idActivityActor;
						this.nameActivityActor = nameActivityActor;
						this.idAbstractionLevel = idAbstractionLevel;
					}

					ActivityActorRecord(ActivityActorRecord aar) {
						this.idActivityActor = aar.idActivityActor;
						this.nameActivityActor = aar.nameActivityActor;
						this.idAbstractionLevel = aar.idAbstractionLevel;
					}
				}
				
				
	}

}
