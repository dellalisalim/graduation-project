package vpcisteamitemspanels;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import gui.VpCISWindow;
import vpcispanels.AllCISProjectsPanel;
import vpcispanels.TemplatePanel;

public class EditLinksPanel extends TemplatePanel{

	JButton addLinkButton;
	JButton deleteLinkButton;
	
	public EditLinksPanel() {
		//constructor
		super("Links Managment");
		//hide useless buttons
		addButton.hide();
		updateButton.hide();
		deleteButton.hide();
		okButton.hide();
		cancelButton.hide();
		editButton.hide();
		
		//configure the center Panel
		addLinkButton = new JButton("Add a Link");
		deleteLinkButton = new JButton ("Delete a Link");
		
		addLinkButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VpCISWindow.pVpCIS.remove(VpCISWindow.pVpCIS.addLinkPanel);
				AddLinkPanel addLinkPanel = new AddLinkPanel();
				VpCISWindow.pVpCIS.add(addLinkPanel, "AddLinkPanel");
				VpCISWindow.pVpCIS.repaint();
				VpCISWindow.pVpCIS.revalidate();
				VpCISWindow.pVpCIS.cl.show(VpCISWindow.pVpCIS, "AddLinkPanel");
			}
		});
		
		deleteLinkButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VpCISWindow.pVpCIS.remove(VpCISWindow.pVpCIS.deleteLinkPanel);
				DeleteLinkPanel deleteLinkPanel = new DeleteLinkPanel();
				VpCISWindow.pVpCIS.add(deleteLinkPanel, "DeleteLinkPanel");
				VpCISWindow.pVpCIS.repaint();
				VpCISWindow.pVpCIS.revalidate();
				VpCISWindow.pVpCIS.cl.show(VpCISWindow.pVpCIS, "DeleteLinkPanel");
			}
		});
		
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VpCISWindow.pVpCIS.remove(VpCISWindow.pVpCIS.allItemsPanel);
				AllItemsPanel allItemsPanel = new AllItemsPanel();
				VpCISWindow.pVpCIS.add(allItemsPanel, "AllItemsPanel");
				VpCISWindow.pVpCIS.repaint();
				VpCISWindow.pVpCIS.revalidate();
				VpCISWindow.pVpCIS.cl.show(VpCISWindow.pVpCIS, "AllItemsPanel");
			}
		});
		
		centerPanel.setLayout(new FlowLayout());
		centerPanel.add(addLinkButton);
		centerPanel.add(deleteLinkButton);
	}

	
}
