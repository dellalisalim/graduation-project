package vpcisteamitemspanels;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import gui.Launcher;
import gui.VpCISWindow;
import vpcispanels.TemplatePanel;

public class AddLinkPanel extends TemplatePanel {

	// links
	JLabel linkLabel;
	JList allLinks;

	// typeLink
	JLabel typeLinkLabel;
	JList typeLinkList;

	// source and the quality if Actor
	JLabel sourceLabel;
	JList sourceList;
	JScrollPane sourceListScrollPane;
	ArrayList<TableRecord> sourceArrayList;

	// destination and the quality if ACtor
	JLabel destinationLabel;
	JList destinationList;
	JScrollPane destinationListScrollPane;
	ArrayList<TableRecord> destinationArrayList;

	final String[] emptyTable = { "                " };

	// variable to save what has been selected
	int idSourceSelected;
	int idDestinationSelected;
	String typeLinkSelected;
	String qualitySourceSelected;
	String qualityDestinationSelected;

	ActionListener okal;

	public AddLinkPanel() {
		// constructor
		super("Add a new Link");

		// hide the useless Buttons
		addButton.hide();
		updateButton.hide();
		deleteButton.hide();
		backButton.hide();
		editButton.hide();

		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// do something
				// get back to EditLinkspanel
				switchToEditLinksPanel();
			}
		});

		okButton.addActionListener(okal = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// DO NOTHING , it's used to redefine the Action listener
				showSystemMessage("Choose a Link to add, or Click 'Cancel' Button to get back to Edit Links");
			}
		});

		// showMessage
		showSystemMessage("Choose first a Type Link you want to add, "
				+ "then choose the Source and Destination, then click 'Ok' to Link them");

		// configure the centerPanel
		centerPanel.setLayout(new FlowLayout());

		// links
		linkLabel = new JLabel("Link :");
		allLinks = new JList();

		// typeLink
		typeLinkLabel = new JLabel("Type Link :");
		typeLinkList = new JList(emptyTable);
		typeLinkList.setEnabled(false);

		// source and the quality if Actor
		sourceLabel = new JLabel("Source :");
		sourceList = new JList(emptyTable);
		sourceList.setEnabled(false);
		sourceListScrollPane = new JScrollPane(sourceList);

		// destination and the quality if Actor
		destinationLabel = new JLabel("Destination :");
		destinationList = new JList(emptyTable);
		destinationList.setEnabled(false);
		destinationListScrollPane = new JScrollPane(destinationList);

		linkLabel = new JLabel("Link :");
		// allLinkList
		String[] links = { "Link Team - Team", 
				"Link Team - Activity Team", 
				"Link Team - Information Team",
				"Link Activity Team - Activity Team",
				"Link Information Team - Information Team",
				"Link Activity Team - Information Team", 
				"Link Actor - Actor",
				"Link Actor - Activity Actor",
				"Link Actor - Information Actor",
				"Link Activity Actor - Activity Actor",
				"Link Information Actor - Information Actor",
				"Link Activity Actor - Information Actor",
				"Link Team - Actor",
				"Link Activity Team - Activity Actor",
				"Link Information Team - Information Actor"};

		allLinks = new JList(links);
		allLinks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		allLinks.setVisibleRowCount(8);

		centerPanel.add(linkLabel); // 0
		centerPanel.add(new JScrollPane(allLinks)); // 1
		centerPanel.add(typeLinkLabel); // 2
		centerPanel.add(typeLinkList); // 3
		centerPanel.add(sourceLabel); // 4
		centerPanel.add(sourceList); // 5
		centerPanel.add(destinationLabel); // 6
		centerPanel.add(destinationList); // 7

		// set the behaviour
		allLinks.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) { // this will prevent double
													// events
					switch (allLinks.getSelectedValue().toString()) {

					case "Link Team - Team": {
						centerPanel.revalidate();
						centerPanel.repaint();
						// Show the Typelink
						showTypeLink();

						// reinitialize typeLinkList
						String[] typeLinks = { "Cooperation", "Sub Team" };
						reinitializeAndFillTypeLinkList(typeLinks);

						// get the records
						sourceArrayList = getRecords("Team");
						destinationArrayList = getRecords("Team");

						// fill the lists
						reinitializeAndFillLists(sourceArrayList, destinationArrayList);

						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								if (emptyLists(sourceList, destinationList)) {
									// at least one list is empty
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionTypeLinkListNull(typeLinkList)) {
									// typeLinkList haven't been selected
									showSystemMessage("Choose a type Link");
								} else if (selectionListsNull(sourceList, destinationList)) {
									// at least one list haven't been selected
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (sameIdSourceIdDestination(idSourceSelected, idDestinationSelected)) {
									// same src and dst = no reflexive links
									showSystemMessage(
											"Same Source and Destination, no reflexive links, Add impossible");
								} else if (linkUnionExists("LinkTeamTeam", "sourceIdTeam", idSourceSelected,
										"destinationIdTeam", idDestinationSelected)) {
									// link already exists
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert4ArgsTypeLink("LinkTeamTeam (typeLink,sourceIdTeam,DestinationIdTeam)",
											typeLinkSelected, idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null, "Link Team - Team added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
						break;
					case "Link Team - Activity Team": {

						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("Team");
						destinationArrayList = getRecords("ActivityTeam");

						reinitializeAndFillLists(sourceArrayList, destinationArrayList);

						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (linkExists("LinkTeamActivityTeam", "sourceIdTeam", idSourceSelected,
										"destinationIdActivityTeam", idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert3Args("LinkTeamActivityTeam (sourceIdTeam,DestinationIdActivityTeam)",
											idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null, "Link Team - Activity Team added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
						break;
					case "Link Team - Information Team": {
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("Team");
						destinationArrayList = getRecords("InformationTeam");

						reinitializeAndFillLists(sourceArrayList, destinationArrayList);

						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (linkExists("LinkTeamInformationTeam", "sourceIdTeam", idSourceSelected,
										"destinationIdInformationTeam", idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert3Args("LinkTeamInformationTeam (sourceIdTeam,DestinationIdInformationTeam)",
											idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Team - Information Team added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
						break;
					case "Link Activity Team - Activity Team": {
						centerPanel.revalidate();
						centerPanel.repaint();
						// Show the Typelink
						showTypeLink();

						// reinitialize typeLinkList
						String[] typeLinks = { "In Relation", "Sub Activity" };
						reinitializeAndFillTypeLinkList(typeLinks);

						// get the records
						sourceArrayList = getRecords("ActivityTeam");
						destinationArrayList = getRecords("ActivityTeam");

						// fill the lists
						reinitializeAndFillLists(sourceArrayList, destinationArrayList);

						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								if (emptyLists(sourceList, destinationList)) {
									// at least one list is empty
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionTypeLinkListNull(typeLinkList)) {
									// typeLinkList haven't been selected
									showSystemMessage("Choose a type Link");
								} else if (selectionListsNull(sourceList, destinationList)) {
									// at least one list haven't been selected
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (sameIdSourceIdDestination(idSourceSelected, idDestinationSelected)) {
									// same src and dst = no reflexive links
									showSystemMessage(
											"Same Source and Destination, no reflexive links, Add impossible");
								} else if (linkUnionExists("LinkActivityTeamActivityTeam", "sourceIdActivityTeam",
										idSourceSelected, "destinationIdActivityTeam", idDestinationSelected)) {
									// link already exists
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert4ArgsTypeLink(
											"LinkActivityTeamActivityTeam (typeLink,sourceIdActivityTeam,DestinationIdActivityTeam)",
											typeLinkSelected, idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Activity Team - Activity Team added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});

					}
						break;
					case "Link Information Team - Information Team": {
						centerPanel.revalidate();
						centerPanel.repaint();
						// Show the Typelink
						showTypeLink();

						// reinitialize typeLinkList
						String[] typeLinks = { "In Relation", "Sub Information" };
						reinitializeAndFillTypeLinkList(typeLinks);

						// get the records
						sourceArrayList = getRecords("InformationTeam");
						destinationArrayList = getRecords("InformationTeam");

						// fill the lists
						reinitializeAndFillLists(sourceArrayList, destinationArrayList);

						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								if (emptyLists(sourceList, destinationList)) {
									// at least one list is empty
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionTypeLinkListNull(typeLinkList)) {
									// typeLinkList haven't been selected
									showSystemMessage("Choose a type Link");
								} else if (selectionListsNull(sourceList, destinationList)) {
									// at least one list haven't been selected
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (sameIdSourceIdDestination(idSourceSelected, idDestinationSelected)) {
									// same src and dst = no reflexive links
									showSystemMessage(
											"Same Source and Destination, no reflexive links, Add impossible");
								} else if (linkUnionExists("LinkInformationTeamInformationTeam",
										"sourceIdInformationTeam", idSourceSelected, "destinationIdInformationTeam",
										idDestinationSelected)) {
									// link already exists
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert4ArgsTypeLink(
											"LinkInformationTeamInformationTeam (typeLink,sourceIdInformationTeam,DestinationIdInformationTeam)",
											typeLinkSelected, idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Information Team - Information Team added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});

					}
						break;
					case "Link Activity Team - Information Team": {
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("ActivityTeam");
						destinationArrayList = getRecords("InformationTeam");

						reinitializeAndFillLists(sourceArrayList, destinationArrayList);

						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (linkExists("LinkActivityTeamInformationTeam", "sourceIdActivityTeam",
										idSourceSelected, "destinationIdInformationTeam", idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert3Args(
											"LinkActivityTeamInformationTeam (sourceIdActivityTeam,DestinationIdInformationTeam)",
											idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Activity Team - Information Team added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
						break;
					case "Link Actor - Actor": {
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getActorRecords();
						destinationArrayList = getActorRecords();

						reinitializeAndFillActorSourceList(sourceArrayList);
						reinitializeAndFillActorDestinationList(destinationArrayList);

						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (sameIdSourceIdDestination(idSourceSelected, idDestinationSelected)) {
									// same src and dst = no reflexive links
									showSystemMessage(
											"Same Source and Destination, no reflexive links, Add impossible");
								} else if (LinkActorUnionExists("LinkActorActor", idSourceSelected,qualitySourceSelected,
										idDestinationSelected,qualityDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert5Args(
											"LinkActorActor (sourceIdActor,sourceQuality,destinationIdActor,destinationQuality)",
											idSourceSelected, qualitySourceSelected, idDestinationSelected,
											qualityDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null, "Link Actor - Actor added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});

					}
						break;
					case "Link Actor - Activity Actor" : {
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getActorRecords();
						destinationArrayList = getRecords("ActivityActor");
						
						reinitializeAndFillActorSourceList(sourceArrayList);
						reinitializeAndFillDestinationList(destinationArrayList);
						
						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (linkActorSourceExists("LinkActorActivityActor", idSourceSelected,qualitySourceSelected,
										"destinationIdActivityActor",idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert4AgrsActorActivInfo("LinkActorActivityActor(sourceIdActor,sourceQuality,destinationIdActivityActor)",
											idSourceSelected, qualitySourceSelected,idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null, "Link Actor - Activity Actor added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
						
					}
					break;
					case "Link Actor - Information Actor" :{
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getActorRecords();
						destinationArrayList = getRecords("InformationActor");
						
						reinitializeAndFillActorSourceList(sourceArrayList);
						reinitializeAndFillDestinationList(destinationArrayList);
						
						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								}  else if (linkActorSourceExists("LinkActorInformationActor", idSourceSelected,qualitySourceSelected,
										"destinationIdInformationActor",idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert4AgrsActorActivInfo("LinkActorInformationActor(sourceIdActor,sourceQuality,destinationIdInformationActor)",
											idSourceSelected, qualitySourceSelected,idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null, "Link Actor - Information Actor added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
					break;
					case "Link Activity Actor - Activity Actor":{
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("ActivityActor");
						destinationArrayList = getRecords("ActivityActor");
						
						reinitializeAndFillLists(sourceArrayList, destinationArrayList);
						
						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (sameIdSourceIdDestination(idSourceSelected, idDestinationSelected)) {
									// same src and dst = no reflexive links
									showSystemMessage(
											"Same Source and Destination, no reflexive links, Add impossible");
								}else if (linkUnionExists("LinkActivityActorActivityActor", "sourceIdActivityActor",
										idSourceSelected, "destinationIdActivityActor", idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert3Args(
											"LinkActivityActorActivityActor (sourceIdActivityActor,DestinationIdActivityActor)",
											idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Activity Team - Activity Team added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
					break;
					case "Link Information Actor - Information Actor" :{
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("InformationActor");
						destinationArrayList = getRecords("InformationActor");
						
						reinitializeAndFillLists(sourceArrayList, destinationArrayList);
						
						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (sameIdSourceIdDestination(idSourceSelected, idDestinationSelected)) {
									// same src and dst = no reflexive links
									showSystemMessage(
											"Same Source and Destination, no reflexive links, Add impossible");
								}else if (linkUnionExists("LinkInformationActorInformationActor", "sourceIdInformationActor",
										idSourceSelected, "destinationIdInformationActor", idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert3Args(
											"LinkInformationActorInformationActor (sourceIdInformationActor,DestinationIdInformationActor)",
											idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Information Actor - Information Actor added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
					break;
					case "Link Activity Actor - Information Actor" :{
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("ActivityActor");
						destinationArrayList = getRecords("InformationActor");
						
						reinitializeAndFillLists(sourceArrayList, destinationArrayList);
						
						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (linkExists("LinkActivityActorInformationActor", "sourceIdActivityActor",
										idSourceSelected, "destinationIdInformationActor", idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert3Args(
											"LinkActivityActorInformationActor (sourceIdActivityActor,DestinationIdInformationActor)",
											idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Activity Actor - Information Actor added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
					break;
					case "Link Team - Actor" :{
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("Team");
						destinationArrayList = getActorRecords();
						
						
						reinitializeAndFillSourceList(sourceArrayList);
						reinitializeAndFillActorDestinationList(destinationArrayList);
						
						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (LinkActorDestinationExists("LinkTeamActor", "sourceIdTeam", idSourceSelected, idDestinationSelected,
										qualityDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert4ArgsTeamActor(
											"LinkTeamActor (sourceIdTeam,destinationIdActor,destinationQuality)",
											idSourceSelected, idDestinationSelected, qualityDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Team - Actor added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
					case "Link Activity Team - Activity Actor" : {
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("ActivityTeam");
						destinationArrayList = getRecords("ActivityActor");
						
						reinitializeAndFillLists(sourceArrayList, destinationArrayList);
						
						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (linkExists("LinkActivityTeamActivityActor", "sourceIdActivityTeam",
										idSourceSelected, "destinationIdActivityActor", idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert3Args(
											"LinkActivityTeamActivityActor (sourceIdActivityTeam,DestinationIdActivityActor)",
											idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Activity Team - Activity Actor added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					}
					break;
					case "Link Information Team - Information Actor" : {
						centerPanel.revalidate();
						centerPanel.repaint();
						hideTypeLink();

						sourceArrayList = getRecords("InformationTeam");
						destinationArrayList = getRecords("InformationActor");
						
						reinitializeAndFillLists(sourceArrayList, destinationArrayList);
						
						okButton.removeActionListener(okal);
						okButton.addActionListener(okal = new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								if (emptyLists(sourceList, destinationList)) {
									showSystemMessage("Source and/or Destination List is empty, Add impossible");
								} else if (selectionListsNull(sourceList, destinationList)) {
									showSystemMessage("Choose both from the Source List and Destination List");
								} else if (linkExists("LinkInformationTeamInformationActor", "sourceIdInformationTeam",
										idSourceSelected, "destinationIdInformationActor", idDestinationSelected)) {
									showSystemMessage(
											"Link between Source and Destination already exists, duplicating link is impossible");
								} else {
									// OK
									// insert into the table
									insert3Args(
											"LinkInformationTeamInformationActor (sourceIdInformationTeam,DestinationIdInformationActor)",
											idSourceSelected, idDestinationSelected);
									// showMessage about successfull add
									showSystemMessage("Link added successfuly");
									// JOptionPane
									JOptionPane.showMessageDialog(null,
											"Link Information Team - Information Actor added successfuly");
									// back to EditLinksPanel
									switchToEditLinksPanel();
								}
							}
						});
					
						
					}
					break;
					default:
						break;
					}
				}
			}
		});

	}

	class TableRecord {

		int id;
		String name;
		String quality;

		TableRecord(int id, String name) {
			this.id = id;
			this.name = name;
		}

		TableRecord(int id, String quality, String name) {
			this.id = id;
			this.quality = quality;
			this.name = name;
		}

	}

	public void insert3Args(String tableName, int source, int destination) {
		try {
			String request = "INSERT INTO " + tableName + " VALUES ('" + source + "','" + destination + "')";
			System.out.println("request : " + request);
			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.st.executeUpdate(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insert4ArgsTypeLink(String tableName, String typeLink, int source, int destination) {
		try {
			String request = "INSERT INTO " + tableName + " VALUES ('" + typeLink + "','" + source + "','" + destination
					+ "')";
			System.out.println("request : " + request);
			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.st.executeUpdate(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insert4ArgsTeamActor(String tableName, int source, int destination, String quality) {
		try {
			String request = "INSERT INTO " + tableName + " VALUES ('" + source + "','" + destination + "','" + quality
					+ "')";
			System.out.println("request : " + request);
			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.st.executeUpdate(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insert4AgrsActorActivInfo(String tableName, int source, String quality, int destination) {
		try {
			String request = "INSERT INTO " + tableName + " VALUES ('" + source + "','" + quality + "','" + destination
					+ "')";
			System.out.println("request : " + request);
			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.st.executeUpdate(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insert5Args(String tableName, int source, String qualitySource, int destination,
			String qualityDestination) {
		try {
			String request = "INSERT INTO " + tableName + " VALUES ('" + source + "','" + qualitySource + "','"
					+ destination + "','" + qualityDestination + "')";
			System.out.println("request : " + request);
			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.st.executeUpdate(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean emptyLists(JList sourceList, JList destinationList) {
		return ((sourceList.getModel().getSize() == 0) || (destinationList.getModel().getSize() == 0)) ? true : false;
	}

	public boolean sameIdSourceIdDestination(int idSourceSelected, int idDestinationSelected) {
		return (idSourceSelected == idDestinationSelected) ? true : false;
	}

	public boolean selectionListsNull(JList sourceList, JList destinationList) {
		return ((sourceList.getSelectedValue() == null) || (destinationList.getSelectedValue() == null)) ? true : false;
	}

	public boolean selectionTypeLinkListNull(JList typeLinkList) {
		return (typeLinkList.getSelectedValue() == null) ? true : false;
	}

	public boolean linkExists(String tableName, String sourceFormat, int source, String destinationFormat,
			int destination) {
		boolean result = false;
		try {

			String request = "SELECT * FROM " + tableName + " WHERE " + sourceFormat + " = '" + source + "' AND "
					+ destinationFormat + " = '" + destination + " ' ";

			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.rs = Launcher.adbc.st.executeQuery(request);

			if (Launcher.adbc.rs.next())
				result = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean linkUnionExists(String tableName, String sourceFormat, int source, String destinationFormat,
			int destination) {
		boolean result = false;
		try {

			String request = "SELECT * FROM " + tableName + " WHERE " + sourceFormat + " = '" + source + "' AND "
					+ destinationFormat + " = '" + destination + " ' " + " UNION " + "SELECT * FROM " + tableName
					+ " WHERE " + sourceFormat + " = '" + destination + "' AND " + destinationFormat + " = '" + source
					+ " ' ";

			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.rs = Launcher.adbc.st.executeQuery(request);

			if (Launcher.adbc.rs.next())
				result = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean linkActorSourceExists(String tableName, int source, String sourceQuality, String destinationFormat,
			int destination) {
		boolean result = false;
		try {
			String request = "SELECT * FROM " + tableName + " WHERE sourceIdActor = '" + source
					+ "' AND sourceQuality = '" + sourceQuality + "' AND " + destinationFormat + " = '" + destination
					+ " ' ";
			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.rs = Launcher.adbc.st.executeQuery(request);

			if (Launcher.adbc.rs.next())
				result = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean LinkActorDestinationExists(String tableName, String sourceFormat, int source, int destination,
			String destinationQuality) {
		boolean result = false;
		try {

			String request = "SELECT * FROM " + tableName + " WHERE " + sourceFormat + " = '" + source
					+ "' AND destinationIdActor = '" + destination + "' AND destinationQuality = '" + destinationQuality
					+ " ' ";
			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.rs = Launcher.adbc.st.executeQuery(request);

			if (Launcher.adbc.rs.next())
				result = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean LinkActorUnionExists(String tableName, int sourceId, String sourceQuality, int destinationId, String destinationQuality ) {
		boolean result = false;
		try {

			String request = "SELECT * FROM " + tableName + " WHERE sourceIdActor = '" + sourceId + "' AND sourceQuality = '" + sourceQuality 
					+ "' AND destinationIdActor = '" + destinationId + "' AND destinationQuality = '" + destinationQuality + " ' "
					+ " UNION "
					+ "SELECT * FROM " + tableName + " WHERE sourceIdActor = '" + destinationId + "' AND sourceQuality = '" + destinationQuality 
					+ "' AND destinationIdActor = '" + sourceId + "' AND destinationQuality = '" + sourceQuality + " ' ";

			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.rs = Launcher.adbc.st.executeQuery(request);

			if (Launcher.adbc.rs.next())
				result = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void reinitializeAndFillActorSourceList(ArrayList<TableRecord> tableActor) {
		centerPanel.remove(sourceList);
		centerPanel.remove(sourceListScrollPane);

		ArrayList<String> tempoList;
		TableRecord tr;
		Iterator<TableRecord> it;

		tempoList = new ArrayList<String>();
		it = tableActor.iterator();
		while (it.hasNext()) {
			tr = it.next();
			tempoList.add(String.valueOf(tr.id) + " - " + tr.name + "(" + tr.quality + ")");
		}
		this.sourceList = new JList(tempoList.toArray());
		this.sourceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.sourceList.setVisibleRowCount(8);
		this.sourceListScrollPane = new JScrollPane(this.sourceList);

		sourceList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) { // this will prevent double
													// events
					// System.out.println(sourceList.getSelectedIndex());
					idSourceSelected = sourceArrayList.get(sourceList.getSelectedIndex()).id;
					qualitySourceSelected = sourceArrayList.get(sourceList.getSelectedIndex()).quality;
					// System.out.println(idSourceSelected);
				}
			}
		});

		centerPanel.add(sourceListScrollPane, 5);

	}

	public void reinitializeAndFillActorDestinationList(ArrayList<TableRecord> tableActor) {
		centerPanel.remove(destinationList);
		centerPanel.remove(destinationListScrollPane);

		ArrayList<String> tempoList;
		TableRecord tr;
		Iterator<TableRecord> it;

		tempoList = new ArrayList<String>();
		it = tableActor.iterator();
		while (it.hasNext()) {
			tr = it.next();
			tempoList.add(String.valueOf(tr.id) + " - " + tr.name + "(" + tr.quality + ")");
		}
		this.destinationList = new JList(tempoList.toArray());
		this.destinationList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.destinationList.setVisibleRowCount(8);
		this.destinationListScrollPane = new JScrollPane(this.destinationList);

		destinationList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) { // this will prevent double
													// events
					// System.out.println(destinationList.getSelectedIndex());
					idDestinationSelected = destinationArrayList.get(destinationList.getSelectedIndex()).id;
					qualityDestinationSelected = destinationArrayList.get(destinationList.getSelectedIndex()).quality;
					// System.out.println(iddestinationSelected);
				}
			}
		});

		centerPanel.add(destinationListScrollPane);

	}

	public void reinitializeAndFillSourceList(ArrayList<TableRecord> tableSource) {
		centerPanel.remove(sourceList);
		centerPanel.remove(sourceListScrollPane);

		// fill the lists
		ArrayList<String> tempoList;
		TableRecord tr;
		Iterator<TableRecord> it;

		// fill the source
		tempoList = new ArrayList<String>();
		it = tableSource.iterator();
		while (it.hasNext()) {
			tr = it.next();
			tempoList.add(String.valueOf(tr.id) + " - " + tr.name);
		}

		this.sourceList = new JList(tempoList.toArray());
		this.sourceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.sourceList.setVisibleRowCount(8);
		this.sourceListScrollPane = new JScrollPane(this.sourceList);

		sourceList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) { // this will prevent double
													// events
					// System.out.println(sourceList.getSelectedIndex());
					idSourceSelected = sourceArrayList.get(sourceList.getSelectedIndex()).id;
					// System.out.println(idSourceSelected);
				}
			}
		});
		centerPanel.add(sourceListScrollPane, 5);
	}

	public void reinitializeAndFillDestinationList(ArrayList<TableRecord> tableDestination) {
		centerPanel.remove(destinationList);
		centerPanel.remove(destinationListScrollPane);

		// fill the lists
		ArrayList<String> tempoList;
		TableRecord tr;
		Iterator<TableRecord> it;

		// fill the destination
		tempoList = new ArrayList<String>();
		it = tableDestination.iterator();
		while (it.hasNext()) {
			tr = it.next();
			tempoList.add(String.valueOf(tr.id) + " - " + tr.name);
		}
		this.destinationList = new JList(tempoList.toArray());
		this.destinationList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.destinationList.setVisibleRowCount(8);
		this.destinationListScrollPane = new JScrollPane(this.destinationList);

		destinationList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) { // this will prevent double
													// events
					idDestinationSelected = destinationArrayList.get(destinationList.getSelectedIndex()).id;
					// System.out.println(idDestinationSelected);
				}
			}
		});

		centerPanel.add(destinationListScrollPane);
	}

	public void reinitializeAndFillLists(ArrayList<TableRecord> tableSource, ArrayList<TableRecord> tableDestination) {

		centerPanel.remove(sourceList);
		centerPanel.remove(sourceListScrollPane);

		centerPanel.remove(destinationList);
		centerPanel.remove(destinationListScrollPane);

		// fill the lists
		ArrayList<String> tempoList;
		TableRecord tr;
		Iterator<TableRecord> it;

		// fill the source
		tempoList = new ArrayList<String>();
		it = tableSource.iterator();
		while (it.hasNext()) {
			tr = it.next();
			tempoList.add(String.valueOf(tr.id) + " - " + tr.name);
		}

		this.sourceList = new JList(tempoList.toArray());
		this.sourceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.sourceList.setVisibleRowCount(8);
		this.sourceListScrollPane = new JScrollPane(this.sourceList);

		sourceList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) { // this will prevent double
													// events
					// System.out.println(sourceList.getSelectedIndex());
					idSourceSelected = sourceArrayList.get(sourceList.getSelectedIndex()).id;
					// System.out.println(idSourceSelected);
				}
			}
		});
		// fill the destination
		tempoList = new ArrayList<String>();
		it = tableDestination.iterator();
		while (it.hasNext()) {
			tr = it.next();
			tempoList.add(String.valueOf(tr.id) + " - " + tr.name);
		}
		this.destinationList = new JList(tempoList.toArray());
		this.destinationList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.destinationList.setVisibleRowCount(8);
		this.destinationListScrollPane = new JScrollPane(this.destinationList);

		destinationList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) { // this will prevent double
													// events
					idDestinationSelected = destinationArrayList.get(destinationList.getSelectedIndex()).id;
					// System.out.println(idDestinationSelected);
				}
			}
		});
		centerPanel.add(sourceListScrollPane, 5);
		centerPanel.add(destinationListScrollPane);

	}

	public void reinitializeAndFillTypeLinkList(String[] typeLinks) {
		// centerPanel.revalidate();
		// centerPanel.repaint();
		centerPanel.remove(typeLinkList);
		// centerPanel.revalidate();
		// centerPanel.repaint();
		typeLinkList = new JList(typeLinks);
		typeLinkList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		typeLinkList.setVisibleRowCount(2);

		typeLinkList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) { // this will prevent double
													// events
					// System.out.println(sourceList.getSelectedIndex());
					typeLinkSelected = typeLinkList.getSelectedValue().toString();
					System.out.println(typeLinkSelected);
				}
			}
		});
		// typeLinkSelected = this.typeLinkList.getSelectedValue().toString();
		/*
		 * typeLinkList.addListSelectionListener(new ListSelectionListener() {
		 * public void valueChanged(ListSelectionEvent arg0) { typeLinkSelected
		 * = typeLinkList.getSelectedValue().toString(); } });
		 */
		centerPanel.add(typeLinkList, 3);
		centerPanel.revalidate();

	}

	public ArrayList<TableRecord> getRecords(String tableName) {

		ArrayList<TableRecord> tempoList = new ArrayList<TableRecord>();
		TableRecord tr;

		try {
			String request = "SELECT * FROM " + tableName;

			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.rs = Launcher.adbc.st.executeQuery(request);

			while (Launcher.adbc.rs.next()) {
				tr = new TableRecord(Launcher.adbc.rs.getInt(1), Launcher.adbc.rs.getString(2));
				tempoList.add(tr);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return tempoList;
	}

	public ArrayList<TableRecord> getActorRecords() {
		ArrayList<TableRecord> tempoList = new ArrayList<TableRecord>();
		TableRecord tr;

		try {
			String request = "SELECT * FROM Actor";
			Launcher.adbc.st = Launcher.adbc.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Launcher.adbc.rs = Launcher.adbc.st.executeQuery(request);
			while (Launcher.adbc.rs.next()) {

				tr = new TableRecord(Launcher.adbc.rs.getInt(1), Launcher.adbc.rs.getString(2),
						Launcher.adbc.rs.getString(3));
				tempoList.add(tr);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tempoList;
	}

	public void hideTypeLink() {
		typeLinkLabel.hide();
		typeLinkList.hide();
	}

	public void showTypeLink() {
		typeLinkLabel.show();
		typeLinkList.show();
	}

	public void switchToEditLinksPanel() {
		VpCISWindow.pVpCIS.remove(VpCISWindow.pVpCIS.editLinksPanel);
		EditLinksPanel editLinksPanel = new EditLinksPanel();
		VpCISWindow.pVpCIS.add(editLinksPanel, "EditLinksPanel");
		VpCISWindow.pVpCIS.repaint();
		VpCISWindow.pVpCIS.revalidate();
		VpCISWindow.pVpCIS.cl.show(VpCISWindow.pVpCIS, "EditLinksPanel");
	}
}
